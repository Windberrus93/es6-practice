
// This file was created in order to follow the given instructions.
console.log('es6.js');

const colors = [
    {
        color: "black",
        category: "hue",
        type: "primary",
        code: {
            rgba: [255, 255, 255, 1],
            hex: "#000"
        }
    },
    {
        color: "white",
        category: "value",
        type: "primary",
        code: {
            rgba: [0, 0, 0, 1],
            hex: "#FFF"
        }
    },
    {
        color: "red",
        category: "hue",
        type: "primary",
        code: {
            rgba: [255, 0, 0, 1],
            hex: "#F00"
        }
    },
    {
        color: "blue",
        category: "hue",
        type: "primary",
        code: {
            rgba: [0, 0, 255, 1],
            hex: "#00F"
        }
    },
    {
        color: "yellow",
        category: "hue",
        type: "primary",
        code: {
            rgba: [255, 255, 0, 1],
            hex: "#FF0"
        }
    },
    {
        color: "green",
        category: "hue",
        type: "secondary",
        code: {
            rgba: [0, 255, 0, 1],
            hex: "#0F0"
        }
    },
];

const addHtml = () => {
    const app = document.getElementById('app');
    const width = (colors && (colors.length > 0)) ? (100 / colors.length) - 1 : 0;
    if (colors) {
        const elements = colors.map(color => {
            const { color: colorName, category, type, code } = color;
            const { hex } = code;
            const fontColor = (colorName === 'black') ? 'white' : 'black';
            return `
            <div class="aligned-info">
                <h1>${colorName}</h1>
                <div class="card" style="background: ${hex}; color: ${fontColor};">
                    <h2>${category}</h2>
                    <p>${type}</p>
                </div>
            </div>`;
        });
        app.innerHTML = elements.join('');
    }
}

window.onload = addHtml;